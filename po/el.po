# Greek translation of deskscribe.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Giannis Katsampirhs <giannis1_86@hotmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: deskscribe\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-03-24 09:22+0200\n"
"PO-Revision-Date: 2008-03-23 19:45+0200\n"
"Last-Translator: Giannis Katsampirhs <giannis1_86@hotmail.com>\n"
"Language-Team: Greek <team@gnome.gr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Language: Greek\n"
"X-Poedit-Country: GREECE\n"

#: ../deskscribe.desktop.in.h:1
msgid "Desktop Activity Logger"
msgstr "Καταγραφέας δραστηριότητας επιφάνειας εργασίας"

#: ../deskscribe.desktop.in.h:2
msgid "Desktop user activity logging tool"
msgstr "Εργαλείο καταγραφής δραστηριότητας επιφάνειας εργασία χρήστη"

#: ../deskscribe.xml.in.h:1
msgid "Mouse Log File"
msgstr "Αρχείο καταγραφής ποντικιού"

#: ../mausgrapher.desktop.in.h:1
msgid "Mouse Click Grapher"
msgstr "Δημιουργός γραφημάτων των κλικ του ποντικιού"

#: ../mausgrapher.desktop.in.h:2
msgid "Mouse click graphing tool"
msgstr "Εργαλείο γραφημάτων των κλικ του ποντικιού"

#: ../src/deskscribe.c:48
msgid "Save Mouse Log"
msgstr "Αποθήκευση καταγραφής ποντικιού"

#: ../src/deskscribe.c:57 ../src/mausgrapher.c:239
msgid "Mouse Log Files"
msgstr "Αρχεία καταγραφής ποντικιού"
