/*
 *  Authors: Rodney Dawes <dobey.pwns@gmail.com>
 *
 *  Copyright 2007 Rodney Dawes
 *  Copyright 2007 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#include <math.h>
#include <stdlib.h>
#include <gtk/gtkentry.h>

#include "mausgrapher-draw.h"
#include "record.h"

gboolean mausgrapher_draw_chart (GtkWidget * widget, GdkEventExpose * event,
				 MausGrapher * grapher) {
  cairo_t * context;
  gint length;
  gint last_x, last_y;
  gint count;
  gdouble radius;
  time_t end;
  GSList * l;

  last_x = last_y = 0;
  length = 0;

  length = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (grapher->entry));

  context = gdk_cairo_create (grapher->canvas->window);

  cairo_set_tolerance (context, 0.0f);

  cairo_set_operator (context, CAIRO_OPERATOR_OVER);

  cairo_rectangle (context, event->area.x, event->area.y,
		   event->area.width, event->area.height);
  cairo_clip (context);

  cairo_set_source_rgba (context, 1.0f, 1.0f, 1.0f, 1.0f);
  cairo_rectangle (context, event->area.x, event->area.y,
		   event->area.width, event->area.height);
  cairo_fill (context);

  count = 0;
  end = 0;
  for (l = grapher->loglist; l && l->data; l = l->next) {
    MauserLogItem * item = l->data;

    radius = 8.0f + (gint) (item->length / 1000) - 1.0f;

    if (count == 0 && length > 0 && item->timestamp != 0)
      end = item->timestamp + length;

    if (length > 0 && item->timestamp > end)
      break;

    cairo_set_source_rgba (context, 0.93f, 0.83f, 0.0f, 1.0f);
    cairo_set_line_width (context, 1.0f);

    cairo_arc (context, (double) item->x, (double) item->y, radius,
	       0.0f, M_PI * 2.0f);
    cairo_fill_preserve (context);

    cairo_set_source_rgba (context, 0.77f, 0.63f, 0.0f, 1.0f);
    cairo_stroke (context);

    cairo_set_source_rgba (context, 0.0f, 0.0f, 0.0f, 1.0f);
    if (last_x != item->x && last_y != item->y && last_x != 0 && last_y != 0) {
      cairo_move_to (context, last_x, last_y);
      cairo_line_to (context, item->x, item->y);
      cairo_move_to (context, item->x, item->y);
      cairo_stroke (context);
    }

    last_x = item->x;
    last_y = item->y;

    count++;
  }
  cairo_destroy (context);

  return TRUE;
}
