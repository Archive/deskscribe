/*
 *  Authors: Rodney Dawes <dobey.pwns@gmail.com>
 *
 *  Copyright 2007 Rodney Dawes
 *  Copyright 2007 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gdk/gdkx.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include "docklet.h"
#include "deskscribe.h"

static void mauser_icon_theme_changed (GtkIconTheme * theme, Mauser * maus) {
  mauser_reset_icon (maus);
}

static void mauser_create (void) {
  Mauser * maus;
  GtkFileFilter * filter;

  maus = g_new0 (Mauser, 1);
  maus->recording = FALSE;
  maus->theme = gtk_icon_theme_get_default ();

  gtk_icon_theme_append_search_path (maus->theme, PKGDATADIR G_DIR_SEPARATOR_S "icons");
  maus->log_list = NULL;

  gtk_window_set_default_icon_name ("mauseverfolger");

  g_signal_connect (maus->theme, "changed",
		    G_CALLBACK (mauser_icon_theme_changed), maus);

  maus->filesel = gtk_file_chooser_dialog_new (_("Save Mouse Log"),
					       NULL,
					       GTK_FILE_CHOOSER_ACTION_SAVE,
					       GTK_STOCK_CANCEL,
					       GTK_RESPONSE_CANCEL,
					       GTK_STOCK_SAVE,
					       GTK_RESPONSE_OK,
					       NULL);
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("Mouse Log Files"));
  gtk_file_filter_add_mime_type (filter, "text/x-mouse-log");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (maus->filesel), filter);
  gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (maus->filesel), filter);

  mauser_docklet_create (maus);
}

gint main (gint argc, gchar ** argv) {
  gtk_init (&argc, &argv);

  mauser_create ();
  gtk_main ();

  return 0;
}
