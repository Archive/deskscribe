/*
 *  Authors: Rodney Dawes <dobey.pwns@gmail.com>
 *
 *  Copyright 2007 Rodney Dawes
 *  Copyright 2007 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#ifndef _MAUSGRAPHER_DRAW_H_
#define _MAUSGRAPHER_DRAW_H_

#include <gtk/gtkwidget.h>

typedef struct _MausGrapher MausGrapher;

struct _MausGrapher {
  GtkWidget * window;
  GtkWidget * canvas;
  GtkWidget * entry;

  GtkWidget * fooser;
  GtkWidget * abox;

  GSList * loglist;
};

gboolean mausgrapher_draw_chart (GtkWidget * widget, GdkEventExpose * event,
				 MausGrapher * grapher);

#endif
