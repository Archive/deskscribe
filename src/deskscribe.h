/*
 *  Authors: Rodney Dawes <dobey.pwns@gmail.com>
 *
 *  Copyright 2007 Rodney Dawes
 *  Copyright 2007 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#ifndef _MAUSEVERFOLGER_H_
#define _MAUSEVERFOLGER_H_

#include <gtk/gtk.h>

typedef struct _Mauser Mauser;

struct _Mauser {
  GtkWidget * docklet;
  GtkWidget * dock_icon;

  GtkWidget * popup;
  GtkWidget * start_item;
  GtkWidget * stop_item;

  GtkWidget * filesel;

  GtkIconTheme * theme;

  GSList * log_list;
  gboolean recording;
};

#endif
