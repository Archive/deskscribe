/*
 *  Authors: Rodney Dawes <dobey.pwns@gmail.com>
 *
 *  Copyright 2007 Rodney Dawes
 *  Copyright 2007 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#ifndef _MAUSER_RECORD_H_
#define _MAUSER_RECORD_H_

#include <time.h>

#include "deskscribe.h"

typedef struct _MauserLogItem MauserLogItem;

struct _MauserLogItem {
  gint x;
  gint y;
  guint button;

  /* windows app measures start to end in milliseconds */
  GTimeVal start;
  GTimeVal end;

  /* app name for window at pointer occurance */
  gchar * appname;

  /* difference between start and end, in milliseconds */
  glong length;

  /* old school */
  time_t timestamp;
};

void mauser_record_stop (Mauser * maus);
void mauser_record_start (Mauser * maus);

void mauser_log_write (Mauser * maus);

#endif
