/*
 *  Authors: Rodney Dawes <dobey.pwns@gmail.com>
 *
 *  Copyright 2007 Rodney Dawes
 *  Copyright 2007 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#include <time.h>

#define WNCK_I_KNOW_THIS_IS_UNSTABLE

#include <gdk/gdkx.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkfilechooser.h>
#include <libwnck/application.h>
#include <libwnck/window.h>

#include "docklet.h"
#include "deskscribe.h"
#include "record.h"

#define MAUSER_TIMEOUT 5

static gint timeout_id;
static GTimeVal tv_count;
static gboolean button_pressed;
static gboolean in_drag;
static gint lx, ly;
static guint lb;
static WnckScreen * screen;

static gboolean mauser_window_timeout (Mauser * maus) {
  GdkDisplay * display;
  GtkSettings * settings;
  MauserLogItem * item = NULL;
  gboolean cur_item = FALSE;
  gboolean drag_end = FALSE;
  gint x, y;
  GdkModifierType mask;
  guint button;
  gint dnd_distance;
  WnckWindow * app = NULL;

  Window root;
  Window xwindow;
  Window child;
  Window xwindow_last = 0;
  int rx, ry, wx, wy;
  unsigned int wmask;

  tv_count.tv_usec += MAUSER_TIMEOUT * 1000;

  if (tv_count.tv_usec >= G_USEC_PER_SEC) {
    tv_count.tv_sec = (tv_count.tv_usec / G_USEC_PER_SEC) + tv_count.tv_sec;
    tv_count.tv_usec = tv_count.tv_usec %  G_USEC_PER_SEC;
  }

  if (!maus->recording) {
    g_source_remove (timeout_id);
    timeout_id = 0;
    return FALSE;
  }

  display = gdk_display_get_default ();

  /* Grab the display to keep the pointer math sane */
  gdk_x11_display_grab (display);

  gdk_display_get_pointer (display, NULL, &x, &y, &mask);

  xwindow = GDK_ROOT_WINDOW ();
  XQueryPointer (GDK_DISPLAY_XDISPLAY (display), xwindow,
		 &root, &child, &rx, &ry, &wx, &wy, &wmask);
  if (root == xwindow)
    xwindow = child;
  else
    xwindow = root;
      
  while (xwindow)
    {
      xwindow_last = xwindow;
      XQueryPointer (GDK_DISPLAY_XDISPLAY (display), xwindow,
		     &root, &xwindow, &rx, &ry, &wx, &wy, &wmask);
    }
  gdk_x11_display_ungrab (display);

  button = 0;

  if (mask & GDK_BUTTON1_MASK)
    button = 1;
  else if (mask & GDK_BUTTON2_MASK)
    button = 2;
  else if (mask & GDK_BUTTON3_MASK)
    button = 3;

  settings = gtk_widget_get_settings (maus->docklet);
  g_object_get (G_OBJECT (settings), "gtk-dnd-drag-threshold", &dnd_distance,
		NULL);

  if (button_pressed) {
    if (button != 0 && button == lb) {
      if (x - lx <= dnd_distance)
	in_drag = TRUE;
      else if (lx - x <= dnd_distance)
	in_drag = TRUE;
      else if (y - ly <= dnd_distance)
	in_drag = TRUE;
      else if (ly - y <= dnd_distance)
	in_drag = TRUE;
    } else {
      if (in_drag) {
	drag_end = TRUE;
      }
      in_drag = FALSE;
    }

    if (in_drag)
      return TRUE;

    button_pressed = FALSE;

    if (!drag_end) {
      if (maus->log_list != NULL)
	item = maus->log_list->data;
      else
	return TRUE;

      if (button == item->button && (x == item->x && y == item->y))
	cur_item = TRUE;
      else
	return TRUE;
    }
  } else {
    if (button == 0)
      return TRUE;

    button_pressed = TRUE;
  }
  if (!drag_end && !cur_item && lx == x && ly == y)
    return TRUE;

  lx = x;
  ly = y;
  lb = button;

  if (!cur_item) {
    item = g_new0 (MauserLogItem, 1);
    item->x = x;
    item->y = y;
    item->button = button;

    item->start.tv_sec = tv_count.tv_sec;
    item->start.tv_usec = tv_count.tv_usec;

    if (child != None)
      app = wnck_window_get (child);

    if (app)
      item->appname = g_strdup (wnck_window_get_icon_name (app));
  }

  item->end.tv_sec = tv_count.tv_sec;
  item->end.tv_usec = tv_count.tv_usec;

  if (!cur_item)
    maus->log_list = g_slist_prepend (maus->log_list, item);

  return TRUE;
}

void mauser_record_stop (Mauser * maus) {

  if (!maus->recording)
    return;

  maus->recording = FALSE;
  mauser_reset_icon (maus);

  gtk_widget_set_sensitive (maus->stop_item, FALSE);
  gtk_widget_set_sensitive (maus->start_item, TRUE);

  mauser_log_write (maus);
}

void mauser_record_start (Mauser * maus) {
  tv_count.tv_sec = 0;
  tv_count.tv_usec = 0;

  lb = lx = ly = 0;

  button_pressed = FALSE;
  in_drag = FALSE;

  maus->recording = TRUE;
  mauser_reset_icon (maus);

  if (!screen)
    screen = wnck_screen_get_default ();

  timeout_id = g_timeout_add (MAUSER_TIMEOUT,
			      (GSourceFunc) mauser_window_timeout, maus);

  gtk_widget_set_sensitive (maus->stop_item, TRUE);
  gtk_widget_set_sensitive (maus->start_item, FALSE);
}

void mauser_log_write (Mauser * maus) {
  GdkScreen * screen;
  gint screen_w, screen_h;
  GSList * l;
  time_t now;
  struct tm * curtime;
  const gchar * tformat;
  gchar filename[256];
  gchar * filepath = NULL;
  FILE * fp;

  screen = gdk_screen_get_default ();
  screen_w = gdk_screen_get_width (screen);
  screen_h = gdk_screen_get_height (screen);

  maus->log_list = g_slist_reverse (maus->log_list);

  now = time (NULL);
  curtime = localtime (&now);

  tformat = "%Y%m%d%H%M.mlog";
  if (!strftime (filename, sizeof (filename), tformat, curtime))
    goto error;

  gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (maus->filesel),
				     filename);
  switch (gtk_dialog_run (GTK_DIALOG (maus->filesel))) {
  case GTK_RESPONSE_OK:
    filepath = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (maus->filesel));
  case GTK_RESPONSE_CANCEL:
  default:
    gtk_widget_hide (maus->filesel);
    break;
  }
  if (!filepath)
    goto error;

  if ((fp = fopen (filepath, "w+")) != NULL) {
    fprintf (fp, "StartTime,EndTime,Type,Mouse,XPos,YPos,UserEntry,Application,Command,Data\n");
    fprintf (fp, "# %dx%d\n", screen_w, screen_h);
    fprintf (fp, "0,0,\"\",\"\",\"\",\"\",\"\",\"\",FPS,200\n");
    fprintf (fp, "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"\n");
    for (l = maus->log_list; l && l->data; l = l->next) {
      MauserLogItem * item = l->data;
      const gchar * btnstr;

      switch (item->button) {
      case 1:
	btnstr = "Left";
	break;
      case 2:
	btnstr = "Middle";
	break;
      case 3:
	btnstr = "Right";
	break;
      default:
	btnstr = "\"\"";
	break;
      }

      fprintf (fp, "%ld,%ld,Mouse,%s,%d,%d,\"\",%s,\"\",\"\"\n",
	       (item->start.tv_sec * 1000) + (item->start.tv_usec / 1000),
	       (item->end.tv_sec * 1000) + (item->end.tv_usec / 1000),
	       btnstr, item->x, item->y,
	       item->appname ? item->appname : "\"\"");
    }
    fclose (fp);
  }

  g_free (filepath);

 error:

  while (maus->log_list != NULL) {
    MauserLogItem * item = maus->log_list->data;

    maus->log_list = g_slist_remove (maus->log_list, item);
    g_free (item->appname);
    g_free (item);
  }
}
