/*
 *  Authors: Rodney Dawes <dobey.pwns@gmail.com>
 *
 *  Copyright 2007 Rodney Dawes
 *  Copyright 2007 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include "mausgrapher-draw.h"
#include "record.h"

#define BUFSIZE 4096

static void mausgrapher_quit (GtkWidget * widget, MausGrapher * grapher) {
  GSList * l;

  gtk_widget_destroy (grapher->fooser);

  for (l = grapher->loglist; l && l->data; l = l->next) {
    MauserLogItem * data = l->data;

    grapher->loglist = g_slist_remove_all (grapher->loglist, data);
    g_free (data);
  }
  g_slist_free (grapher->loglist);

  g_free (grapher);

  gtk_main_quit ();
}

static gboolean mausgrapher_delete_event (GtkWidget * widget,
					  GdkEventAny * event,
					  MausGrapher * grapher) {
  mausgrapher_quit (widget, grapher);

  return TRUE;
}

static void mausgrapher_entry_activated (GtkSpinButton * entry,
					 MausGrapher * grapher) {
  gtk_widget_queue_draw (grapher->canvas);
}

static void mausgrapher_load_data (MausGrapher * grapher,
				   const gchar * filename) {
  FILE * fp;
  char buffer[BUFSIZE];
  gboolean old_data = FALSE;
  GSList * l;

  for (l = grapher->loglist; l && l->data; l = l->next) {
    MauserLogItem * data = l->data;

    grapher->loglist = g_slist_remove_all (grapher->loglist, data);
    g_free (data);
  }
  g_slist_free (grapher->loglist);
  grapher->loglist = NULL;

  if ((fp = fopen (filename, "r")) != NULL) {
    gint w, h;
    gint lx, ly;
    gint count = 0;

    lx = ly = count = 0;
    while (fgets (buffer, BUFSIZE, fp)) {
      char ** csvdata;
      MauserLogItem * item;

      if (buffer[0] == '#') {
	if (sscanf (buffer, "# %dx%d", &w, &h) == 2) {
	  gtk_widget_set_size_request (grapher->canvas, w, h);
	  if (count == 0)
	    old_data = TRUE;
	}
	continue;
      }

      csvdata = g_strsplit (buffer, ",", -1);

      if (strcmp (csvdata[0], "StartTime") == 0)
	goto loopout;

      item = g_new0 (MauserLogItem, 1);
      if (old_data) {
	item->x = atoi (csvdata[1]);
	item->y = atoi (csvdata[2]);
	item->timestamp = atol (csvdata[0]);
      } else {
	if (strcmp (csvdata[2], "Mouse") != 0) {
	  g_free (item);
	  goto loopout;
	}
	item->x = atoi (csvdata[4]);
	item->y = atoi (csvdata[5]);

	item->start.tv_sec = atol (csvdata[0]) / 1000;
	item->start.tv_usec = (atol (csvdata[0]) % 1000) * 1000;

	item->end.tv_sec = atol (csvdata[1]) / 1000;
	item->end.tv_usec = (atol (csvdata[1]) % 1000) * 1000;

	item->length = atol (csvdata[1]) - atol (csvdata[0]);
      }

      if (lx == item->x && ly == item->y) {
	g_free (item);
	goto loopout;
      }

      grapher->loglist = g_slist_prepend (grapher->loglist, item);

      lx = item->x;
      ly = item->y;

    loopout:
      /* increment the line count, and free the parsed data */
      count++;

      g_strfreev (csvdata);
    }
    fclose (fp);

    grapher->loglist = g_slist_reverse (grapher->loglist);
  }
  gtk_widget_queue_draw (grapher->canvas);
}

static void mausgrapher_open_log (GtkWidget * widget, MausGrapher * grapher) {
  gchar * filename;

  switch (gtk_dialog_run (GTK_DIALOG (grapher->fooser))) {
  case GTK_RESPONSE_OK:
    filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (grapher->fooser));
    mausgrapher_load_data (grapher, filename);
    g_free (filename);
  case GTK_RESPONSE_CANCEL:
  default:
    gtk_widget_hide (grapher->fooser);
    break;
  }
}

static void mausgrapher_about (GtkWidget * widget, MausGrapher * grapher) {
  switch (gtk_dialog_run (GTK_DIALOG (grapher->abox))) {
  default:
    gtk_widget_hide (grapher->abox);
    break;
  }
}

static GladeXML * mausgrapher_load_glade (void) {
  GladeXML * new;
  gchar * filename;

  filename = g_build_filename (PKGDATADIR, "mausgrapher.glade", NULL);
  if (!g_file_test (filename, G_FILE_TEST_EXISTS)) {
    g_free (filename);
    filename = g_build_filename (g_get_current_dir (),
				 "mausgrapher.glade", NULL);
  }
  new = glade_xml_new (filename, NULL, NULL);
  g_free (filename);

  return new;
}

static void mausgrapher_create (const gchar * filename) {
  MausGrapher * grapher;
  GladeXML * glade;
  GtkWidget * oitem, * obutton, * qitem, *aitem;
  GtkFileFilter * filter;

  gtk_window_set_default_icon_name ("mausgrapher");

  grapher = g_new0 (MausGrapher, 1);

  glade = mausgrapher_load_glade ();

  grapher->window = glade_xml_get_widget (glade, "MausGrapher");

  grapher->entry = glade_xml_get_widget (glade, "spinbutton1");

  g_signal_connect (grapher->entry, "value-changed",
		    G_CALLBACK (mausgrapher_entry_activated), grapher);

  grapher->canvas = glade_xml_get_widget (glade, "drawingarea1");
  gtk_widget_show (grapher->canvas);

  if (filename)
    mausgrapher_load_data (grapher, filename);

  gtk_widget_show_all (grapher->window);

  g_signal_connect (grapher->canvas, "expose-event",
		    G_CALLBACK (mausgrapher_draw_chart), grapher);

  g_signal_connect (grapher->window, "delete-event",
		    G_CALLBACK (mausgrapher_delete_event), grapher);

  oitem = glade_xml_get_widget (glade, "open1");
  g_signal_connect (oitem, "activate",
		    G_CALLBACK (mausgrapher_open_log), grapher);

  obutton = glade_xml_get_widget (glade, "toolbar-open");
  g_signal_connect (obutton, "clicked",
		    G_CALLBACK (mausgrapher_open_log), grapher);

  qitem = glade_xml_get_widget (glade, "quit1");
  g_signal_connect (qitem, "activate",
		    G_CALLBACK (mausgrapher_quit), grapher);

  aitem = glade_xml_get_widget (glade, "about1");
  g_signal_connect (aitem, "activate",
		    G_CALLBACK (mausgrapher_about), grapher);

  grapher->fooser = glade_xml_get_widget (glade, "fooser");
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("Mouse Log Files"));
  gtk_file_filter_add_mime_type (filter, "text/x-mouse-log");
  gtk_file_filter_add_pattern (filter, "*.mlog");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (grapher->fooser), filter);
  gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (grapher->fooser), filter);

  grapher->abox = glade_xml_get_widget (glade, "aboutbox");
}

gint main (gint argc, gchar ** argv) {
  gtk_init (&argc, &argv);

  if (argc >= 2 && argv[1])
    mausgrapher_create (argv[1]);
  else
    mausgrapher_create (NULL);

  gtk_main ();

  return 0;
}
