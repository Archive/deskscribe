/*
 *  Authors: Rodney Dawes <dobey.pwns@gmail.com>
 *
 *  Copyright 2007 Rodney Dawes
 *  Copyright 2007 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

#include "docklet.h"
#include "eggtrayicon.h"
#include "record.h"

static gboolean mauser_docklet_closed (GtkWidget * widget,
				       GdkEventAny * event,
				       Mauser * maus) {
  gtk_widget_destroy (maus->docklet);
  mauser_docklet_create (maus);

  return FALSE;
}

static gboolean mauser_button_release (GtkWidget * widget,
				       GdkEventButton * event,
				       Mauser * maus) {
  GdkDisplay * display;

  display = gtk_widget_get_display (widget);

  switch (event->button) {
  case 1:
    switch (event->type) {
    case GDK_2BUTTON_PRESS:
      break;
    default:
      break;
    }
    break;
  case 3:
    gtk_menu_popup (GTK_MENU (maus->popup), NULL, widget,
		    NULL, maus, event->button, GDK_CURRENT_TIME);
    break;
  default:
    break;
  }

  return FALSE;
}

static void mauser_docklet_start (GtkMenuItem * item, Mauser * maus) {
  mauser_record_start (maus);
}

static void mauser_docklet_stop (GtkMenuItem * item, Mauser * maus) {
  mauser_record_stop (maus);
}

static void mauser_docklet_quit (GtkMenuItem * item, Mauser * maus) {
  mauser_record_stop (maus);
  gtk_main_quit ();
}

void mauser_docklet_create (Mauser * maus) {
  GtkWidget * ebox;
  GtkWidget * menuitem;

  maus->docklet = GTK_WIDGET (egg_tray_icon_new ("Mouse Tracker"));

  ebox = gtk_event_box_new ();
  gtk_widget_set_size_request (ebox, 24, 24);
  gtk_container_add (GTK_CONTAINER (maus->docklet), ebox);
  gtk_widget_show (ebox);

  g_signal_connect (G_OBJECT (ebox), "button_release_event",
		    G_CALLBACK (mauser_button_release), maus);

  g_signal_connect (G_OBJECT (maus->docklet), "delete_event",
		    G_CALLBACK (mauser_docklet_closed), maus);

  maus->dock_icon = gtk_image_new ();
  gtk_container_add (GTK_CONTAINER (ebox), maus->dock_icon);
  gtk_widget_show (maus->dock_icon);

  maus->popup = gtk_menu_new ();

  maus->start_item = gtk_image_menu_item_new_from_stock (GTK_STOCK_MEDIA_RECORD,
							 NULL);
  gtk_menu_shell_append (GTK_MENU_SHELL (maus->popup), maus->start_item);
  gtk_widget_show (maus->start_item);

  g_signal_connect (maus->start_item, "activate",
		    G_CALLBACK (mauser_docklet_start), maus);

  maus->stop_item = gtk_image_menu_item_new_from_stock (GTK_STOCK_MEDIA_STOP,
							NULL);
  gtk_menu_shell_append (GTK_MENU_SHELL (maus->popup), maus->stop_item);
  gtk_widget_set_sensitive (maus->stop_item, FALSE);
  gtk_widget_show (maus->stop_item);

  g_signal_connect (maus->stop_item, "activate",
		    G_CALLBACK (mauser_docklet_stop), maus);

  menuitem = gtk_separator_menu_item_new ();
  gtk_menu_shell_append (GTK_MENU_SHELL (maus->popup), menuitem);
  gtk_widget_show (menuitem);

  menuitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT, NULL);
  gtk_menu_shell_append (GTK_MENU_SHELL (maus->popup), menuitem);
  gtk_widget_show (menuitem);

  g_signal_connect (menuitem, "activate",
		    G_CALLBACK (mauser_docklet_quit), maus);

  gtk_widget_show (maus->popup);

  mauser_reset_icon (maus);

  gtk_widget_show (maus->docklet);
}

void mauser_reset_icon (Mauser * maus) {
  GdkPixbuf * pixbuf;
  gint panel_w, panel_h, icon_size;

  gtk_window_get_size (GTK_WINDOW (gtk_widget_get_toplevel (maus->docklet)),
		       &panel_w, &panel_h);

  icon_size = MIN (panel_w, panel_h);
  icon_size += 2;

  if (icon_size < 22)
    icon_size = 16;
  else if (icon_size >= 22 && icon_size < 32)
    icon_size = 22;
  else if (icon_size >= 32 && icon_size < 48)
    icon_size = 32;

  if (!maus->recording)
    pixbuf = gtk_icon_theme_load_icon (maus->theme, "deskscribe",
				       icon_size, 0, NULL);
  else
    pixbuf = gtk_icon_theme_load_icon (maus->theme, "deskscribe-recording",
				       icon_size, 0, NULL);

  if (!pixbuf)
    pixbuf = gtk_icon_theme_load_icon (maus->theme, "image-missing",
				       icon_size, 0, NULL);

  if (pixbuf) {
    gtk_image_set_from_pixbuf (GTK_IMAGE (maus->dock_icon), pixbuf);
    g_object_unref (pixbuf);
  }
}
